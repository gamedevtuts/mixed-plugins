/*:
 * @plugindesc v1.0 - Gives the possibility to define Skills which need Element Mana from a Mana Pool
 * @author Gilles Meyer <admin[at]gamedev-tutorials.com>
 *
 *
 * @param Skill Window Label
 * @desc Which Label should be shown in the Battle Skill List
 * @default Mana Pool
 *
 * @param Skill Window Label Color
 * @desc Which color should the Skil Window Label have (can be a number or a word which looks up in Window_Base (like Window_Base.prototype.normalColor, then you write normal)
 * @default system
 *
 * @param Reinitialize Points each Battle
 * @desc Should the Mana Pool Points be set to 0 before each battle?
 * @type boolean
 * @default true
 *
 * @param Mana Pool active on Start
 * @desc Should all heroes have the Mana Pool at beginning of the game or do you want to activate it yourself
 * @type boolean
 * @default true
 *
 * @param Icon List
 * @desc Icon Ids for each Element (0 for no icon)
 * @default 77,64,65,66,67,68,69,70,71,72,73
 *
 * @help
 *   NOTETAGS:
 *
 * Notetags for Character Class:
 * <magicpool:Fire,Wind,Light>   <-- Defines which Mana Pool Elements this Class can manage
 *
 * Notetags for Skills:
 *  Increasing Points on Skill use:
 * <magicpoolinc:2> <--- If this is used in Element skill with the element fire, the Mana Pool is increased by 2 by using this skill
 * <magicpoolinc:1 Fire> <-- You can decide what Element should be increased if needed
 * <magicpoolinc:2 Fire, 1 Ice> <--- Even Multi Element is allowed
 *
 * Consume points on Skill use:
 * <magicpool:2 Fire> <-- Needs 2 Fire Mana in Mana Pool
 * <magicpool:2 Fire, 1 Ice> <-- Even Multi Element Usage is possible
 *
 *
 * SCRIPT CALLS:
 * Activate/Deactivate Mana Pool for one Actor:
 * GDT.ElementPool.activateManaPool(SOMEACTOR OR ACTORS, true or false)
 * Example: GDT.ElementPool.activateManaPool($gameActors._data[1], true) <-- activate for First Actor
 *
 * Add Mana to Pool via Script and Skips Empty Characters or Characters without Mana Pool, or Characters without that Element:
 * GDT.ElementPool.addPoolMana(ACTOR OR ACTORS, ELEMENTNAME OR ELEMENTID, NUMBER TO RAISE)
 * Example: GDT.ElementPool.addPoolMana($gameActors._data[1], "Ice", 3) <-- Raises the Ice Mana Pool for this Actor + 3
 *
 * HINT:
 * Compatible Script Calls with GDT_ItemScripts:
 * <callscript:GDT.ElementPool.addPoolManaItem("Ice", 2)>
 *
 * LICENSE:
 *  You may use this plugin in commercial and noncommercial way.
 *  It would be nice if you mention my work in your credits and also give me hint,
 *  where you used my work, so i can check out your awesome game ;-)
 *
 */

if(typeof GDT == "undefined") {
    GDT = {};
}
if(typeof GDT.ElementPool == "undefined") {
    GDT.ElementPool = {};
}
if(typeof GDT.Param == "undefined") {
    GDT.Param = {};
}
if(typeof GDT.Param.ElementPool == "undefined") {
    GDT.Param.ElementPool = {};
}

var parameters = PluginManager.parameters('GDT_ElementPool');
GDT.Param.ElementPool.IconList = [null].concat(String(parameters['Icon List']).split(","));
GDT.Param.ElementPool.ManaPoolLabel = String(parameters['Skill Window Label']);
GDT.Param.ElementPool.ManaPoolLabelColor = String(parameters['Skill Window Label Color']);
GDT.Param.ElementPool.ManaPoolReinitAfterBattle = (String(parameters['Reinitialize Points each Battle']) == "true");
GDT.Param.ElementPool.ManaPoolActiveOnStart = (String(parameters['Mana Pool active on Start']) == "true");

GDT.ElementPool.addPoolManaItem = function(targets, item, element, count ) {
    if(!targets instanceof Array) return false;
    GDT.ElementPool.addPoolMana(targets, element, count);
};

GDT.ElementPool.addPoolMana = function(targets, elementOrId, count ) {
    if(!(targets instanceof Array))  {
        targtes = [targets];
    }
    for(var i=0; i < targets.length; i++) {
        var target = targets[i];
        if(!target || !target._magicpool) {
            continue;
        }
        var magicpool = target._magicpool;
        elementOrId = (typeof elementOrId == "number") ? $dataSystem.elements[elementOrId] : elementOrId;
        if(magicpool[element] === undefined) {
            continue;
        }
        magicpool[element] += count;
    }
};

GDT.ElementPool.activateManaPool = function(targets, onOff) {
    if(!(targets instanceof Array)) {
        targets = [targets]
    }
    for(var i=0; i < targets.length; i++) {
        var target = targets[i];
        target && (target._magicpoolactive = !!onOff)
    }
};

var Game_BattlerBase__meetsSkillConditions = Game_BattlerBase.prototype.meetsSkillConditions;
Game_BattlerBase.prototype.meetsSkillConditions = function(skill) {
    var meetsCondition = Game_BattlerBase__meetsSkillConditions.call(this,skill);
    if(meetsCondition && skill.meta && skill.meta.magicpool) {
        var elements = skill.meta.magicpool.split(",");
        return meetsMagicPoolCondition(this._magicpool, elements, skill);
    }
    return meetsCondition;
};

function meetsMagicPoolCondition(magicpool, elements, skill) {
    var meets = true;
    if(!magicpool) {
        return false;
    }
    for(var element of elements) {
        element = element.trim();
        var elementCount = parseInt(element.split(" ")[0], 10);
        var elementName = element.split(" ").splice(1).join(" ");
        console.log(skill.name, magicpool, element);
        var poolRessource = magicpool[elementName];
        if(!poolRessource || poolRessource < elementCount) {
            return false;
        }
    }
    return meets;
}

var Game_Battler__onBattleStart = Game_Battler.prototype.onBattleStart;
Game_Battler.prototype.onBattleStart = function() {
    Game_Battler__onBattleStart.call(this);
    this.firstInitializeMagicpool();
    this.initializeMagicpool();
};

Game_Battler.prototype.firstInitializeMagicpool = function() {
  if(this._magicpoolactive === undefined) {
      this._magicpoolactive = GDT.Param.ElementPool.ManaPoolActiveOnStart;
  }
};

Game_Battler.prototype.initializeMagicpool = function () {
    if(!this.isActor()) {
       return false;
    }
    var actorClass = this.currentClass();
    if(!actorClass.meta || !actorClass.meta.magicpool) {
     return false;
    }

    // Maybe no reinit
    if(this._magicpool && GDT.Param.ElementPool.ManaPoolReinitAfterBattle == false) {
        return false;
    }
    this._magicpool = {};

    var elements = actorClass.meta.magicpool.split(",");
    for(var element of elements)  {
       this._magicpool[element.trim()] = 0;
    }
};

var Game_BattlerBase__initMembers = Game_BattlerBase.prototype.initMembers;
Game_BattlerBase.prototype.initMembers = function() {
    Game_BattlerBase__initMembers.call(this);
};

var Game_Action__apply = Game_Action.prototype.apply;
Game_Action.prototype.apply = function(target) {
    Game_Action__apply.call(this, target);
    if(this.isMagicSkill() && this.item().meta && this.item().meta.magicpoolinc) {
       var skill = this.item();
       var skillMagicpoolInc = this.item().meta.magicpoolinc;
       var skillMagicpoolIncs = skillMagicpoolInc.split(",");

       for(var i=0; i < skillMagicpoolIncs.length; i++) {
           var skillMagicpoolIncArr = skillMagicpoolIncs[i].trim().split(" ");
           var elementName = $dataSystem.elements[skill.damage.elementId];
           var elemValue = skillMagicpoolIncArr[0];
           if(skillMagicpoolIncArr.length > 1) {
               elementName = skillMagicpoolIncArr.splice(1).join(" ");
           }
           if(!isNaN(parseInt(elemValue))) {
               if(typeof this.subject()._magicpool[elementName] !== "undefined") {
                   this.subject()._magicpool[elementName] = parseInt(this.subject()._magicpool[elementName], 10) + parseInt(elemValue, 10);
               }
           }
       }



    }

    if(this.isMagicSkill() && this.item().meta && this.item().meta.magicpool) {
        var skillMagicpool = this.item().meta.magicpool;
        var skillElements = skillMagicpool.split(",");
        for(var skillElement of skillElements) {
            var count = skillElement.trim().split(" ")[0];
            var name = skillElement.trim().split(" ").splice(1).join(" ");
            this.subject()._magicpool[name] = parseInt(this.subject()._magicpool[name], 10) - count[0];
        }
    }
};

// Scene and Windows

GDT.ElementPool.isPoolActiveForActor = function(actor) {
    if(actor) {
       return !!actor._magicpoolactive;
    }
    return false;
};

Window_BattleSkill.prototype.setSkillPoolWindow = function(skillPoolWindow)  {
  this._skillPoolWindow =  skillPoolWindow;
};

var Window_BattleSkill__hide = Window_BattleSkill.prototype.hide;
Window_BattleSkill.prototype.hide = function() {
    Window_BattleSkill__hide.call(this);
    this._skillPoolWindow && this._skillPoolWindow.hide(this);
};

var Window_BattleSkill__show = Window_BattleSkill.prototype.show;
Window_BattleSkill.prototype.show = function() {
    var scene = SceneManager._scene;
    var wy = scene._helpWindow.y + scene._helpWindow.height;
    this.height = scene._statusWindow.y - wy;
    if(GDT.ElementPool.isPoolActiveForActor(this._actor)) {
        this.height -= this.fittingHeight(3);
        this._skillPoolWindow && this._skillPoolWindow.show(this);
        this._skillPoolWindow && this._skillPoolWindow.refresh();
    }
    this.refresh();
    Window_BattleSkill__show.call(this);
};

var Scene_Battle__createAllWindows = Scene_Battle.prototype.createAllWindows;
Scene_Battle.prototype.createAllWindows = function() {
    Scene_Battle__createAllWindows.call(this);
    this._skillWindow.height -=  this._skillWindow.fittingHeight(3);
    this.createSkillManaPoolWindow.call(this);
    this._skillWindow.setSkillPoolWindow(this._skillManaPoolWindow);
};

Scene_Battle.prototype.createSkillManaPoolWindow = function() {
    var wy = this._skillWindow.y + this._skillWindow.height;
    var wh = this._statusWindow.y - wy;
    this._skillManaPoolWindow = new Window_BattleSkillManaPool(0, wy, Graphics.boxWidth, wh);
    this.addWindow(this._skillManaPoolWindow);
};

function Window_BattleSkillManaPool() {
    this.initialize.apply(this, arguments);
}

var Window_SkillList__setActor = Window_SkillList.prototype.setActor;
Window_SkillList.prototype.setActor = function(actor) {
    Window_SkillList__setActor.call(this, actor);
    if(!this._skillPoolWindow) {
        return true;
    }
    this._skillPoolWindow.setActor(actor);
    this._skillPoolWindow.refresh();
};

Window_BattleSkillManaPool.prototype = Object.create(Window_Base.prototype);
Window_BattleSkillManaPool.prototype.constructor = Window_BattleSkill;

Window_BattleSkillManaPool.prototype.setActor = function(actor) {
    this._actor = actor;
};

Window_BattleSkillManaPool.prototype.itemWidth = function() {
    return Math.floor((this.width - this.padding * 2 +
        this.spacing()) / this.elementsPerLine() - this.spacing());
};
Window_BattleSkillManaPool.prototype.itemHeight = function() {
    return this.lineHeight();
};

Window_BattleSkillManaPool.prototype.spacing = function() {
    return 12;
};

Window_BattleSkillManaPool.prototype.refresh = function() {
    var index = 1;
    if(!this._actor) {
        return true;
    }
    this.contents.clearRect(0, 0, this._width, this._height);

    var color = (isNaN(parseInt(GDT.Param.ElementPool.ManaPoolLabelColor, 10))) ? this[GDT.Param.ElementPool.ManaPoolLabelColor+"Color"]() : this.textColor(parseInt(GDT.Param.ElementPool.ManaPoolLabelColor));

    this.changeTextColor(color);
    this.drawText(GDT.Param.ElementPool.ManaPoolLabel,0,0, this._width, "left");
    this.changeTextColor(this.normalColor());
    for(var key in this._actor._magicpool) {
        var value = this._actor._magicpool[key];
        var elemIndex = $dataSystem["elements"].indexOf(key);
        if(elemIndex < 0) {
            elemIndex = 0;
        } else {
            elemIndex = GDT.Param.ElementPool.IconList[elemIndex]  || 0;
        }
        this.drawElement(index, value, elemIndex);
        index++;

    }
};

Window_BattleSkillManaPool.prototype.elementsPerLine = function() {
  return 4;
};

Window_BattleSkillManaPool.prototype.drawElement = function(index, text, elemIndex) {
    var elemsPerLine = this.elementsPerLine();
    var itemsBefore = (index - 1) % elemsPerLine;
    var lineNumber = Math.floor((index-1)/elemsPerLine) + 1;
    var leftIcon = (itemsBefore * this.itemWidth());
    var leftText = leftIcon + Window_Base._iconWidth + this.spacing();
    var top = lineNumber * this.itemHeight();

    elemIndex = parseInt(elemIndex, 10);
    if(elemIndex) {
        this.drawIcon(elemIndex, leftIcon, top);
    }
    this.drawText(text, leftText, top, this.itemWidth(), "left");
};

Window_BattleSkillManaPool.prototype.initialize = function(x, y, width, height) {
    Window_Base.prototype.initialize.call(this, x, y, width, height);
    this.hide();
    this.refresh();
};