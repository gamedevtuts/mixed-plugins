(function() {
    //Kampfmenü
var onBattleStartMenu = Window_PartyCommand.prototype.makeCommandList;
Window_PartyCommand.prototype.makeCommandList = function(){
    onBattleStartMenu.call(this, arguments);
    this.addCommand("Autofight",  'autoClass',$gameParty.members().length>1);
}
    //Kampfmenü
var inBattleStartMenu = Scene_Battle.prototype.createPartyCommandWindow;
Scene_Battle.prototype.createPartyCommandWindow = function(){
    inBattleStartMenu.call(this, arguments);
    this._partyCommandWindow.setHandler('autoClass', this.commandSolo.bind(this));
}

//Im Hauptmenü
var _SceneMenu_createCommandWindow = Scene_Menu.prototype.createCommandWindow;
Scene_Menu.prototype.createCommandWindow = function() {
    _SceneMenu_createCommandWindow.call(this);
    this._commandWindow.setHandler('autoClass',	this.commandSolo.bind(this));
};

//Im Hauptmenü
Window_MenuCommand.prototype.addOriginalCommands = function() {
    this.addCommand("Perks", 'perks', $gameSystem.isPerksCommandEnabled());
    this.addCommand("Autofight", 'autoClass', $gameParty.members().length>1);
};

Scene_Base.prototype.commandSolo = function() {
    if (!$gameParty.inBattle()){
        this._statusWindow.setFormationMode(false);
        this._statusWindow.selectLast();
        this._statusWindow.activate();
        this._statusWindow.setHandler('ok',     this.onSoloOk.bind(this));
        this._statusWindow.setHandler('cancel', this.onPersonalCancel.bind(this));
    }else{
        //var action = BattleManager.inputtingAction();
        //action.setItem(19);
        //this.onSelectAction();
        //this._partyCommandWindow.deactivate();
        this._autoFightSelectionWindow.setup();
        this._autoFightSelectionWindow.activate();

    }
};

Scene_Base.prototype.onSoloOk = function() {
    var index = this._statusWindow.index();
    var actor = $gameParty.members()[index];
    if (actor._classId > 1){
        if ((actor._classId%2)===0){
            $gameActors.actor(actor._actorId).changeClass(actor._classId+1, true);
        }else{
            $gameActors.actor(actor._actorId).changeClass(actor._classId-1, true);
        }
    }else{
        SoundManager.playBuzzer();
    }
    this._statusWindow.refresh()
    this._statusWindow.activate();
};


    // KAMPFMENÜ WINDOW
    function Window_AutoFightSelectionCommand() {
        this.initialize.apply(this, arguments);
    }

    Window_AutoFightSelectionCommand.prototype = Object.create(Window_Command.prototype);
    Window_AutoFightSelectionCommand.prototype.constructor = Window_AutoFightSelectionCommand;

    Window_AutoFightSelectionCommand.prototype.initialize = function() {
        //var y = Graphics.boxHeight - this.windowHeight();
        var y = 0;
        Window_Command.prototype.initialize.call(this, 0, y);
        this.openness = 0;
        this.deactivate();
    };

    Window_AutoFightSelectionCommand.prototype.windowWidth = function() {
        return 192;
    };

    Window_AutoFightSelectionCommand.prototype.numVisibleRows = function() {
        return 4;
    };

    Window_AutoFightSelectionCommand.prototype.makeCommandList = function() {
        for(var member of $gameParty.members()) {
            this.addCommand(`${member.name()} - ${member.currentClass().name}`,  'fight');
        }

    };

    Window_AutoFightSelectionCommand.prototype.setup = function() {
        this.clearCommandList();
        this.makeCommandList();
        this.refresh();
        this.select(0);
        this.activate();
        this.open();
    };    


    // SCENE BATTLE ADD AUTOFIGHT WINDOW
    var Scene_Battle_createAllWindows = Scene_Battle.prototype.createAllWindows;
    Scene_Battle.prototype.createAllWindows = function() {
        Scene_Battle_createAllWindows.call(this);
        this._autoFightSelectionWindow = new Window_AutoFightSelectionCommand();
        this.addWindow.call(this,this._autoFightSelectionWindow);
    };


})();
