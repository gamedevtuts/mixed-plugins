/*:
* @plugindesc v1.0 Monsters Use Item
* @author Gamedev-Tutorials
*
* @param mpasitemcount
* @text MP as Item Count
* @desc When Item Skill should MP condition be used as item Count?
* @type boolean
* @default true
*
* @help
*  This is skill makes it possible for enemies to have items and use them in battle
*  To make it more easier you define Skills which only represent items
*  You can define via Notetag which items an enemy should have at the start
*
* Notetags:
*
* Skills:
*  <isitem:x>
*  Replace x with the Item Id which this Skill represents
*  Example:
*    <isitem: 3>  <-- Skill is really item with id 3
*
* Enemy:
*  <hasitem: itemId,number>
*  What Item does the enemy own at Battle Start
*  Example:
*   <hasitem: 3, 10>  <-- Enemy has the Item with id 3 ten times in its inventory
*  Example 2 (needs GDT_Core):
*    <hasitem: 3, 10>
*    <hasitem: 7, 2>
*    It is possible to add multiple hasItem Notetags for one Enemy thanks to GDT_Cores Note Parsing.
*
*/

GDT.Parameters = PluginManager.parameters('GDT_MonstersUseItem');
GDT.Param = GDT.Param || {};

GDT.Param.mpasitemcount = GDT.Parameters['mpasitemcount'] == "true";

Game_Action.prototype.setEnemyAction = function(action) {
    if (action && action.itemId) {
        this.setItem(action.itemId);
    } else if (action) {
        this.setSkill(action.skillId);
    } else {
        this.clear();
    }
};

Game_Enemy.prototype.isActionValid = function(action) {
    var cond = this.meetsCondition(action);
    if(!cond) {
        return false;
    }
    var canuse = (action.itemId) ? this.canUse($dataItems[action.itemId]) : this.canUse($dataSkills[action.skillId]);
    return canuse;
};


Game_BattlerBase.prototype.meetsItemConditions = function(item) {
    if(this.isEnemy()) {
        return this.meetsUsableItemConditions(item) && this.hasItem(item);
    } else {
        return this.meetsUsableItemConditions(item) && $gameParty.hasItem(item);
    }
};

var Game_Enemy__setup = Game_Enemy.prototype.setup;
Game_Enemy.prototype.setup = function(enemyId, x, y) {
    Game_Enemy__setup.call(this, enemyId, x, y);
    this._items = {};
    this.giveStartItems();
};

Game_Enemy.prototype.giveStartItems = function() {
    var enemy = $dataEnemies[this._enemyId];
    var meta = (GDT.Core && GDT.Core.version >= 10) ? enemy.meta2 : enemy.meta;
    if(meta['hasitem']) {
       var items =  (!(meta['hasitem'] instanceof Array)) ? [meta['hasitem']] : meta['hasitem'];
       for(var i=0; i < items.length; i++) {
           var item = items[i].trim();
           var idAndValue = item.split(",");
           var id = parseInt(idAndValue[0].trim(), 10);
           var value = parseInt(idAndValue[1].trim(), 10);
           this._items[id] = value;
       }
    }
};

Game_Enemy.prototype.hasItem = function(item) {
    if (this.numItems(item) > 0) {
        return true;
    } else {
        return false;
    }
};

Game_Enemy.prototype.numItems = function(item) {
    var container = this.itemContainer(item);
    return container ? container[item.id] || 0 : 0;
};

Game_Enemy.prototype.itemContainer = function(item) {
    if (!item) {
        return null;
    } else if (DataManager.isItem(item)) {
        return this._items;
    }  else {
        return null;
    }
};

Game_Battler.prototype.consumeItem = function(item) {
    if(this.isEnemy()) {
        this.consumeItem(item);
    } else {
        $gameParty.consumeItem(item);
    }

};

Game_Enemy.prototype.consumeItem = function(item) {
    if (DataManager.isItem(item) && item.consumable) {
        this.loseItem(item, 1);
    }
};

Game_Enemy.prototype.maxItems = function(item) {
    return 99;
};

Game_Enemy.prototype.loseItem = function(item, amount) {
    this.gainItem(item, -amount);
};

Game_Enemy.prototype.gainItem = function(item, amount) {
    var container = this.itemContainer(item);
    if (container) {
        var lastNumber = this.numItems(item);
        var newNumber = lastNumber + amount;
        container[item.id] = newNumber.clamp(0, this.maxItems(item));
        if (container[item.id] === 0) {
            delete container[item.id];
        }
        $gameMap.requestRefresh();
    }
};

var DataManager__extractMetadata = DataManager.extractMetadata;
DataManager.extractMetadata = function(data) {
    DataManager__extractMetadata(data);
   if(GDT.Core && GDT.Core.version >= 10) {
       GDT.Core.extractData(data);
   }
};


var DataManager__onLoad = DataManager.onLoad;
DataManager.onLoad = function(object) {
    DataManager__onLoad.call(this,object);
    if((object === $dataEnemies && $dataSkills)  || (object === $dataSkills && $dataEnemies)) {
        for(var i=1; i < $dataEnemies.length; i++) {
            var enemy = $dataEnemies[i];
            for(var actionIndex = 0; actionIndex < enemy.actions.length; actionIndex++ ) {
                var action = enemy.actions[actionIndex];
                var skill = $dataSkills[action.skillId];
                if(skill.meta && skill.meta["isitem"]) {
                    var itemid = parseInt(skill.meta["isitem"].trim(), 10);
                    delete(action.skillId);
                    action.itemId = itemid;
                    if(GDT.Param.mpasitemcount) {
                        this.mapMPConditionToItemCondition(action);
                    }
                }
            }

        }
    }
};

DataManager.mapMPConditionToItemCondition = function(action) {
    if(action.conditionType === 3 && action.itemId)  {
       action.conditionType = 7;
       action.conditionParam1 *= 100;
       action.conditionParam2 *= 100;
    }
};

Game_Enemy.prototype.meetsItemCondition = function(itemId, param1, param2) {
    var itemLength = (this._items[itemId]) ? this._items[itemId] : 0;
    return itemLength >= param1 && itemLength <= param2;
};

var Game_Enemy__meetsCondition = Game_Enemy.prototype.meetsCondition;
Game_Enemy.prototype.meetsCondition = function(action) {
    var cond = Game_Enemy__meetsCondition.call(this, action);
    var param1 = action.conditionParam1;
    var param2 = action.conditionParam2;
    switch (action.conditionType) {
        case 7:
            return this.meetsItemCondition(action.itemId, param1, param2);
        default:
            return cond;
    }
};