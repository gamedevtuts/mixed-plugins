GDT = GDT || {};
GDT.Friendship = GDT.Friendship || {};


/*:
* @plugindesc v1.0 Activates Friendship Levels
* @author Gamedev-Tutorials
*
* @param Experience per Level
* @desc How many Friendship Experience points does a character need to gain a friendshiplevel
* @type number
* @default 10
*
* @param Maximum Friend Level
* @desc What is the maximum Friendshiplevel
* @type number
* @default 5
*
*/

// Overwrite Data

// Load the Config for Friendship
DataManager._databaseFiles.push({
    name : '$dataFriendship',
    src : 'Friendship.json'
});

$gameFriendship = null;

// Creates $gameFriendship Variable at Start
GDT.Friendship.DataManager_createGameObjects = DataManager.createGameObjects;
DataManager.createGameObjects = function() {
    GDT.Friendship.DataManager_createGameObjects();
    $gameFriendship = new GDT_Game_Friendship();
};


// Scene_Map
// First time a Map is loaded, the $gameFriendship will be filled with Data
GDT.Friendship.Scene_Map_onMapLoaded = Scene_Map.prototype.onMapLoaded;
Scene_Map.prototype.onMapLoaded = function() {
    $gameFriendship.setup();
    GDT.Friendship.Scene_Map_onMapLoaded.call(this);
};


// Parameters
GDT.Parameters = PluginManager.parameters('GDT_Friendship');
GDT.Param = GDT.Param || {};

GDT.Param.ExperiencePerLevel = parseInt(GDT.Parameters['Experience per Level'], 10);
GDT.Param.MaximumFriendLevel = parseInt(GDT.Parameters['Maximum Friend Level'], 10);

function GDT_Game_Friendship() {
    this.initialize.apply(this, arguments);
}

GDT_Game_Friendship.prototype.initialize = function() {
    this._friends = {};
    this._initializedFriends = false;
};

// Get Each friend out of the config and create a new Friend Object in $gameFriendship for it
GDT_Game_Friendship.prototype.initializeFriends = function() {
    for(var friendname in $dataFriendship.friends) {
        var friend = $dataFriendship.friends[friendname];
        $gameFriendship._friends[friendname] = $gameFriendship._friends[friendname] || { "level" : friend.startlevel || 0, exp: 0};
    }
};

// Do setup only once
GDT_Game_Friendship.prototype.setup = function(force) {
    if(this._initializedFriends && !force) {
        return null;
    }
    this._initializedFriends = true;

    this.initializeFriends();
    this.initializeVariableData();
};

GDT_Game_Friendship.prototype.getFriend = function(name) {
    if(this._friends[name]) {
        var dynamicData = this._friends[name];
        var staticData = ($dataFriendship && $dataFriendship.friends) && $dataFriendship.friends[name] || {};
        var mixedData = Object.assign({ "name" : name}, staticData, dynamicData);
        return mixedData;
    }
    return null;
};


// PUBLIC FUNCTIONS

GDT_Game_Friendship.prototype.getLevel = function(name) {
    var friend = this.getFriend(name);
    return friend && friend.level || null;
};


GDT_Game_Friendship.prototype.setFriendshipLevel = function(name, level, exp) {
    var friend = this.getFriend(name);
    if (friend) {
        if(level < 0) {
            level = 0;
        }
        if(level > GDT.Param.MaximumFriendLevel) {
            level = GDT.Param.MaximumFriendLevel;
        }
        $gameFriendship._friends[name]["level"] = level;
        $gameFriendship._friends[name]["exp"] = exp || 0;
        this.syncLevelWithVariable(name);
    }
};

GDT_Game_Friendship.prototype.increaseFriendshipLevel = function(name, amount) {
    this.setFriendshipLevel(name, this.getLevel(name) + ((typeof amount == "number") && amount || 1));
};

GDT_Game_Friendship.prototype.decreaseFriendshipLevel = function(name, amount) {
    this.setFriendshipLevel(name, this.getLevel(name) - ((typeof amount == "number") && amount || 1));
};

GDT_Game_Friendship.prototype.gainExp = function(name, xp) {
    var whenLevelUp = GDT.Param.ExperiencePerLevel;
    var friend = this.getFriend(name);
    var leveledup = false;

    if(this.canGainExp(name, xp)) {
        friend.exp += xp;
        var levelups = Math.floor(friend.exp / whenLevelUp);
        var restxp = friend.exp % whenLevelUp;
        if(levelups > 0) {
            this.increaseFriendshipLevel(name, levelups);
            leveledup = true;
        }  else if(levelups < 0) {
            this.decreaseFriendshipLevel(name, levelups);
            leveledup = true;
        }
        this.setExpValue(name, restxp);
    }
    return leveledup;
};

GDT_Game_Friendship.prototype.canGainExp = function(name, xp) {
    var friend = this.getFriend(name);
    if(friend && xp > 0) {
        return friend.level < GDT.Param.MaximumFriendLevel;
    }
    if(friend && xp < 0) {
        return friend.level <= 1;
    }
    return false;
};

// Is used by gainExp
GDT_Game_Friendship.prototype.setExpValue = function(name, value) {
    if(this._friends[name]["level"] >= GDT.Param.MaximumFriendLevel) {
        value = 0;
    }
    this._friends[name]["exp"] = value;
};

GDT_Game_Friendship.prototype.initializeVariableData = function() {
    for(var friendname in this._friends) {
        var friend = this.getFriend(friendname);
        if(friend && typeof friend.variableid === "number") {
            $gameVariables.setValue(friend.variableid, friend.level);
        }
    }
};

GDT_Game_Friendship.prototype.syncLevelWithVariable = function(name) {
    var friend = this.getFriend(name);
    if(friend && friend.variableid) {
        if($gameVariables.value(friend.variableid) != friend.level) {
            $gameVariables.setValue(friend.variableid, friend.level, true);
        }
    }
};


// Game_Variables
GDT.Friendship.Game_Variables_setValue = Game_Variables.prototype.setValue;
Game_Variables.prototype.setValue = function(variableId, value, silent) {
    GDT.Friendship.Game_Variables_setValue.call(this, variableId, value);
    if(!silent) {
        $gameFriendship.setFriendShipLevelByVariable(variableId, value);
    }
};

GDT_Game_Friendship.prototype.setFriendShipLevelByVariable = function(id, level) {
    var friend = this.getFriendByVariableId(id);
    if(friend) {
        $gameFriendship._friends[friend.name]["level"] = level;
    }
};

GDT_Game_Friendship.prototype.getFriendByVariableId = function(id) {
    for(var friendname in $dataFriendship.friends) {
        var friend = $dataFriendship.friends[friendname];
        if(friend && friend.variableid == id) {
            return this.getFriend(friendname);
        }
    }
    return null;
};


var Window_Base__convertEscapeCharacters = Window_Base.prototype.convertEscapeCharacters;
Window_Base.prototype.convertEscapeCharacters = function(text) {
    var text = Window_Base__convertEscapeCharacters.call(this, text);

    text = text.replace(/\x1bF\[(\w+),\s?(\w+)\]/gi, function() {
        var friend = $gameFriendship.getFriend(arguments[1]);
        if(friend) {
            if(typeof friend[arguments[2]] === "undefined") {
                console.log(`Could not find attribute ${arguments[2]} on friend ${arguments[1]}`);
                return "";
            }
            return friend[arguments[2]];
        } else {
            console.log(`Friend: ${arguments[1]} was not found`);
            return "";
        }
    }.bind(this));

    return text;
};








