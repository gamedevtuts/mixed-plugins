/*:
 * @plugindesc v1.0.0 Allow Item to last only for a specific Time (steps, fights, time, use)
 * @author Gilles Meyer <admin[at]gamedev-tutorials.com>
 *
 * @param checktime
 * @text Will you check Items for time? Needs most performance
 * @type boolean
 * @default true
 *
 *
 * @param escapefightisfighting
 * @text Should an escaped fight count as fight?
 * @type boolean
 * @default true
 *
 * @help
 * This script allows you to make items expire.
 * If you only define a durability the item will be lost, when it expires.
 * You can also choose that the item should change into some other item
 *
 * For example: Apple is transformed after 200 steps into a bad apple
 *
 * The only thing you have to do is simply put notetags in the item.
 *
 * IMPORTANT: An Item can only have one condition for expiring.
 * For Example combining steps and fight condition is not possible!
 *
 * NOTETAGS:
 *
 * // After 25 seconds (but at least one step) item will be lost
 * <durability-time:25>
 *
 * // After 10 Steps the item will be lost
 * <durability-steps:10>
 *
 * // After 3 Fights the item will be lost
 * <durability-fight:3>
 * 
 * // After 2 uses of this item it will be lost (item needs to be set to not consumeable)
 * <duarbility-use:2>
 *
 * // When this item is lost it will change into item with the id 11
 * <durability-changeto:11>
 *
 * Example:
 *
 *   Apple will change into bad Apple (id 11) after 200 steps
 *   -------------------------
 *
 *   <durability-steps:200>
 *   <durability-changeto:11>
 *  -------------------------
 *
 *   REQUIREMENTS: NO DEPENDING SCRIPTS!
 *
 *
 */

if(typeof GDT == "undefined") {
    GDT = {};
}
if(typeof GDT.ItemDurability == "undefined") {
    GDT.ItemDurability = {};
}
if(typeof GDT.Param == "undefined") {
    GDT.Param = {};
}
if(typeof GDT.Param.ItemDurability == "undefined") {
    GDT.Param.ItemDurability = {};
}
GDT.ItemDurability.STATE = {
  FIGHT: "durability-fight",
  STEPS: "durability-steps",
  TIME: "durability-time",
  USE: "durability-use"
};
GDT.ItemDurability.CHANGETO = "durability-changeto";
GDT.ItemDurability._playtimelastcheck = 0;

GDT.ItemDurability._items = {};

var parameters = PluginManager.parameters('GDT_ItemDurability');
GDT.Param.ItemDurability.checktime  = (parameters['checktime'] == "true");
GDT.Param.ItemDurability.escapefightisfighting = (parameters['escapefightisfighting'] == "true");

GDT.ItemDurability.processItem = function(item, amount) {
  if(GDT.ItemDurability.getDurabilityType(item)) {
      //console.log("process item", item, amount);
      if(amount > 0) {
          GDT.ItemDurability.gainItem(item, amount)
      } else if (amount < 0) {
          GDT.ItemDurability.loseItem(item, amount);
      }
  }
};

GDT.ItemDurability.loseItem = function(item, amount) {
    var value = GDT.ItemDurability.getDurabilityValue(item);
    amount *= -1;

    GDT.ItemDurability.createItemGroup(item);

    var durableItem = GDT.ItemDurability.getDurabilityStats(item);

    // First use the once that duration do not last long
    for(var i=1; i <= value; i++) {
       if(durableItem[i] >= amount) {
           durableItem[i] -= amount;
           break;
       } else if(durableItem[i] > 0) {
         amount -= durableItem[i];
         durableItem[i] = 0;
       }
       if(amount === 0) { break; }
    }
};

GDT.ItemDurability.gainItem = function(item, amount) {
    var type = GDT.ItemDurability.getDurabilityType(item);
    var value = GDT.ItemDurability.getDurabilityValue(item);

    GDT.ItemDurability.createItemGroup(item);
    GDT.ItemDurability.getDurabilityStats(item)[value] += amount;
};

GDT.ItemDurability.getDurabilityStats = function(item) {
    var type = GDT.ItemDurability.getDurabilityType(item);

    GDT.ItemDurability.createItemGroup(item);
    return GDT.ItemDurability._items[item.id][type];
};

GDT.ItemDurability.createItemGroup = function(item) {
    if(!GDT.ItemDurability._items[item.id]) {
        GDT.ItemDurability._items[item.id] = {};
        var type = GDT.ItemDurability.getDurabilityType(item);
        var value = GDT.ItemDurability.getDurabilityValue(item);
        GDT.ItemDurability._items[item.id][type] = [];
        if(value){
           var durableStats = GDT.ItemDurability.getDurabilityStats(item);
          for(var i=1; i <= value; i++) {
              durableStats[i] = 0;
          }
        }
    }
};

GDT.ItemDurability.increase = function(increaseType) {
  var items = GDT.ItemDurability._items;
  for(var id in items) {
     var stats = items[id][increaseType];
     if(!stats) { continue; }

     var maxLength = stats.length - 1;
     GDT.ItemDurability.expireItem(id, stats[1]);
     // All Values get down shifted. The last one is set to 0
     for(var i=2; i <= maxLength; i++) {
         stats[i-1] = stats[i];
         if(i === maxLength){
             stats[i] = 0;
         }
     }
  }
};

GDT.ItemDurability.expireItem = function(id, amount) {
    if(amount == 0) {
        return;
    }
    var item = $dataItems[id];
    var changeTo = (item.meta[GDT.ItemDurability.CHANGETO])
        ? parseInt(item.meta[GDT.ItemDurability.CHANGETO].trim())
        : false;

    $gameParty.loseItem(item, amount);
    GDT.ItemDurability.loseItem(item, amount);

    if(changeTo){
        $gameParty.gainItem($dataItems[changeTo], amount);
    }
};

GDT.ItemDurability.getDurabilityType = function(item) {
    if(!item || !item.meta) {
        return null;
    }
    for(var name in GDT.ItemDurability.STATE) {
        var value = item.meta[GDT.ItemDurability.STATE[name]];
        if(value) {
            return name.toLowerCase();
        }
    }
    return null;
};

GDT.ItemDurability.getDurabilityValue = function(item) {
    if(!item || !item.meta) {
        return null;
    }
    for(var name in GDT.ItemDurability.STATE) {
        var value = item.meta[GDT.ItemDurability.STATE[name]];
        if(value) {
            return parseInt(value.trim());
        }
    }
    return null;
};

GDT.ItemDurability.checkPlayTime = function() {
    if(GDT.ItemDurability._playtimelastcheck < $gameSystem.playtime() ) {
        var aheadTime = ($gameSystem.playtime() - GDT.ItemDurability._playtimelastcheck);
        if(aheadTime > 0) {
            GDT.ItemDurability._playtimelastcheck = $gameSystem.playtime();
            for(var i=0; i < aheadTime; i++) {
                GDT.ItemDurability.increase("time");
            }
        }
    }
};


GDT.ItemDurability._Game_Party_increaseSteps = Game_Party.prototype.increaseSteps;
Game_Party.prototype.increaseSteps = function() {
    GDT.ItemDurability._Game_Party_increaseSteps.call(this);
    GDT.ItemDurability.increase("steps");
    if(GDT.Param.ItemDurability.checktime) {
        GDT.ItemDurability.checkPlayTime();
    }
};

GDT.ItemDurability._Game_System_onBattleWin = Game_System.prototype.onBattleWin;
Game_System.prototype.onBattleWin = function() {
    GDT.ItemDurability._Game_System_onBattleWin.call(this);
    GDT.ItemDurability.increase("fight");
};
GDT.ItemDurability._Game_System_onBattleEscape = Game_System.prototype.onBattleEscape;
Game_System.prototype.onBattleEscape = function() {
    GDT.ItemDurability._Game_System_onBattleEscape.call(this);
    if(GDT.Param.ItemDurability.escapefightisfighting) {
        GDT.ItemDurability.increase("fight");
    }
};

GDT.ItemDurability._Game_Party_gainItem = Game_Party.prototype.gainItem;
Game_Party.prototype.gainItem = function(item, amount, includeEquip) {
    GDT.ItemDurability._Game_Party_gainItem.call(this, item, amount, includeEquip);
    GDT.ItemDurability.processItem(item, amount);
};

GDT.Game_Party_consumeItem = Game_Party.prototype.consumeItem;
Game_Party.prototype.consumeItem = function(item) {
    GDT.Game_Party_consumeItem.call(this, item)
    if (DataManager.isItem(item) && !item.consumable) {
        GDT.ItemDurability.increase("use");
    }
};
