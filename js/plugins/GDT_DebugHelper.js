/*:
*  @plugindesc v1.0 - Helps you with some Debug Parameters
 * @author Gilles Meyer <admin[at]gamedev-tutorials.com>
 *
 *
 * @param getMapParameter
 * @text Map Start Name of the GET-Parameter
 * @desc Map Start Name of the GET-Parameter
 * @default startMap
 *
 * @param namedMaps[]
 * @type struct<Map>[]
 * @text List of Start Maps
 *
 * @help
 *
 *  Debugger Help:
 *  Press Pos1: Show/Hide Debug Window on Map
 *  Press Page Up:
 *  Move Debug to Upper Position / Toggle Left or Right (if not in full width)
 *  Press Page Down:
 *  Move Debug to Bottom Position / Toggle Left or Right (if not in full width)
 *  Press End:
 *  Toggle Full Width Mode
*/

/*~struct~Map:
 *
 * @param label
 * @type text
 * @text Map GET Label
 *
 * @param nameOrId
 * @type text
 * @text Name or Id of Map
 *
 * @param x
 * @text X Coordinate
 * @type number
 * @min 1
 * @max 99
 *
 * @param y
 * @text Y Coordinate
 * @type number
 * @min 1
 * @max 99
 */

if(typeof GDT == "undefined") {
    GDT = {};
}
if(typeof GDT.DebugHelper == "undefined") {
    GDT.DebugHelper = {};
}
if(typeof GDT.Param == "undefined") {
    GDT.Param = {};
}
if(typeof GDT.Param.DebugHelper == "undefined") {
    GDT.Param.DebugHelper = {};
}

var parameters = PluginManager.parameters('GDT_DebugHelper');
GDT.Param.DebugHelper.getParameter = String(parameters['getMapParameter']) || "startMap";
GDT.Param.DebugHelper.listOfMaps = JSON.parse(parameters['namedMaps[]']) || [];



GDT.DebugHelper.getQueryParameter = function() {
    var params = {};
    if(location.search.length > 1) {
        var keyValues = location.search.substr(1).split("&");
        for(var i=0; i < keyValues.length; i++) {
            var keyValueArray = keyValues[i].split("=");
            params[keyValueArray[0]] = keyValueArray[1];
        }
        return params[GDT.Param.DebugHelper.getParameter] || "";
    }
    return "";
};

GDT.DebugHelper.visitMap = function(startMap) {
  if(startMap.length < 1 ) {
      return false;
  }
  if(startMap.split(",").length === 3) {
      var idXY = startMap.split(",");
      var id = GDT.DebugHelper.getMapIdByName(idXY[0]);
      var x = parseInt(idXY[1], 10);
      var y = parseInt(idXY[2], 10);
      if(id === "") {
          return false;
      }
      $gamePlayer.reserveTransfer(id,x, y);
      return true;
  } else {
      var map = GDT.DebugHelper.startmaps[startMap];
      if(!map) {
          return false;
      }
      var id = GDT.DebugHelper.getMapIdByName(map.nameOrId);
      var x = parseInt(map.x, 10);
      var y = parseInt(map.y, 10);
      if(!id) {
          return false;
      }
      $gamePlayer.reserveTransfer(id,x, y);
      return true;
  }
};
GDT.DebugHelper.createStartMapObject = function() {
    var startMaps = {};
    for(var i=0; i < GDT.Param.DebugHelper.listOfMaps.length; i++) {
        var map = JSON.parse(GDT.Param.DebugHelper.listOfMaps[i]);
        startMaps[map.label] = map;
    }
    return startMaps;
};

GDT.DebugHelper.createMapObject = function() {
   var maps = {};
   for(var i=0; i < $dataMapInfos.length; i++) {
       var map = $dataMapInfos[i];
       if(!map) {
           continue;
       }
       maps[map["name"]] = map.id
   }
   return maps;
};
GDT.DebugHelper.getMapIdByName = function(idOrName) {
  var asNumber = parseInt(idOrName);
  if(!isNaN(asNumber)) {
      return asNumber;
  }
  return GDT.DebugHelper.maps[idOrName] || "";
};


DataManager.setupNewGame = function() {
    this.createGameObjects();
    this.selectSavefileForNewGame();
    $gameParty.setupStartingMembers();

    GDT.DebugHelper.maps = GDT.DebugHelper.createMapObject();
    GDT.DebugHelper.startmaps = GDT.DebugHelper.createStartMapObject();

    var startMap = GDT.DebugHelper.getQueryParameter();
    if(!GDT.DebugHelper.visitMap(startMap)) {
        $gamePlayer.reserveTransfer($dataSystem.startMapId,
            $dataSystem.startX, $dataSystem.startY);
    }
    Graphics.frameCount = 0;
};
Input.keyMapper[35] = 'end';
Input.keyMapper[36] = 'pos1';
Input.keyMapper[80] = 'p';

var GDT_Scene_Map_createAllWindows = Scene_Map.prototype.createAllWindows;
Scene_Map.prototype.createAllWindows = function() {
    GDT_Scene_Map_createAllWindows.call(this);
    this.createGDTDebugWindow();
    
};

Scene_Map.prototype.createGDTDebugWindow = function() {
    this._gdtDebugWindow = new Window_GDTDebug();
    this.addChild(this._gdtDebugWindow);
};

Scene_Map.prototype.isGDTDebugCalled = function() {
    return Input.isTriggered('pos1') && $gameTemp.isPlaytest();
};

Scene_Map.prototype.shouldMoveGDTDebugDown = function() {
    return Input.isTriggered('pagedown') && this._gdtDebugWindow._opened;
};

Scene_Map.prototype.shouldMoveGDTDebugUp = function() {
    return Input.isTriggered('pageup') && this._gdtDebugWindow._opened;
};

Scene_Map.prototype.shouldMakeGDTDebugFullWidth = function() {
    return Input.isTriggered('end') && this._gdtDebugWindow._opened;
};

Scene_Map.prototype.shouldPrintGDTEvent = function() {
    return Input.isTriggered('p') && this._gdtDebugWindow._opened && this._gdtDebugWindow.getEventInfo();
};




var GDT_Scene_Map_updateCallDebug = Scene_Map.prototype.updateCallDebug;
Scene_Map.prototype.updateCallDebug = function() {
    GDT_Scene_Map_updateCallDebug.call(this);
    this.updateCallGDTDebug();
};

Scene_Map.prototype.updateCallGDTDebug = function() {
    if (this.isGDTDebugCalled()) {
        (this._gdtDebugWindow._opened) ? this._gdtDebugWindow.close() : this._gdtDebugWindow.open();
    }
    if(this.shouldMoveGDTDebugDown()) {
        var height = this._gdtDebugWindow.windowHeight();
        var newY = Graphics.boxHeight-height;

        if(this._gdtDebugWindow.y == newY && !this._gdtDebugWindow._fullSize) {
            if(this._gdtDebugWindow.x != 0) {
                this._gdtDebugWindow.x = 0;
            } else {
                this._gdtDebugWindow.x = Graphics.boxWidth / 2;
            }
        } else {
            this._gdtDebugWindow.y = newY;
        }
        this._gdtDebugWindow.refreshWindowPosition(true);
    }
    if(this.shouldMoveGDTDebugUp()) {
        if(this._gdtDebugWindow.y == 0 && !this._gdtDebugWindow._fullSize) {
           if(this._gdtDebugWindow.x != 0) {
               this._gdtDebugWindow.x = 0;
           } else {
               this._gdtDebugWindow.x = Graphics.boxWidth / 2;
           }
        } else {
            var newY = 0;
            this._gdtDebugWindow.y = newY;
        }

        this._gdtDebugWindow.refreshWindowPosition(true);
    }

    if(this.shouldMakeGDTDebugFullWidth()) {
        this._gdtDebugWindow._fullSize = !this._gdtDebugWindow._fullSize;
        this._gdtDebugWindow.refreshWindowPosition(true);
    }

    if(this.shouldPrintGDTEvent()) {
       console.debug(this._gdtDebugWindow._currentEvent);
    }
};




function Window_GDTDebug() {
    this.initialize.apply(this, arguments);
}

Window_GDTDebug.prototype = Object.create(Window_Base.prototype);
Window_GDTDebug.prototype.constructor = Window_GDTDebug;

Window_GDTDebug.prototype.initialize = function() {
    this._opened = false;
    this._fullSize = true;
    this._currentEvent = null;
    var wight = this.windowWidth();
    var height = this.windowHeight(true);
    Window_Base.prototype.initialize.call(this, 0, Graphics.boxHeight-height, wight, height);
    this.opacity = 0;
    this.contentsOpacity = 0;
    this._showCount = 0;
    this.refresh();
};

Window_GDTDebug.prototype.resetData = function() {
    this._opened = false;
    this._fullSize = true;
    this._currentEvent = null;
    this.opacity = 0;
    this.contentsOpacity = 0;
    this._showCount = 0;
};

Window_GDTDebug.prototype.windowWidth = function() {
    return (this._fullSize == true) ? Graphics.boxWidth : (Graphics.boxWidth / 2);
    //return Graphics.boxWidth;
};

Window_GDTDebug.prototype.windowHeight = function(force) {
    return  this.fittingHeight((this.getEventInfo() || force) ? 7 : 2);
};


Window_GDTDebug.prototype.update = function() {
    Window_Base.prototype.update.call(this);
    if(this._opened) {
        if (SceneManager.isSceneChanging()) {
            this.resetData();
        } else {
            this.refresh();
        }
    }
    if (this._showCount > 0 && this._opened) {
        this.updateFadeIn();
        this._showCount--;
    }
    if(!this._opened && this._showCount > 0) {
        this.updateFadeOut();
    } else {
        this.deactivate();
    }
};

Window_GDTDebug.prototype.updateFadeIn = function() {
    this.contentsOpacity += 16;
};

Window_GDTDebug.prototype.updateFadeOut = function() {
    this.contentsOpacity -= 16;
};

Window_GDTDebug.prototype.open = function() {
    this.refresh();
    this._opened = true;
    this._showCount = 150;
};

Window_GDTDebug.prototype.close = function() {
    this.refresh();
    this._opened = false;
    this._showCount = 150;
};

Window_GDTDebug.prototype.refreshWindowPosition = function(doRefresh) {
    var newStats = {};
    newStats.height = this.windowHeight();
    newStats.y = (this.y == 0) ? 0 : Graphics.boxHeight - this.height;
    newStats.x = (this._fullSize && this.x != 0) ? 0 : this.x;
    //newStats.width = this.windowWidth();

    for(var key in newStats) {
        if(this[key] != newStats[key]) {
            this[key] = newStats[key];
        }
    }

    if(doRefresh) {
        this.refresh();
    }
};

Window_GDTDebug.prototype.refresh = function() {
    var evt = this.getEventInfo();
    if(evt != this._currentEvent) {
        this.refreshWindowPosition();
    }
    this.contents.clear();
    this.drawBackground(0, 0 , this.windowWidth(), this.windowHeight());

    this.drawTextLine(1,"MAP ID: "+$gameMap._mapId);
    this.drawTextLine(2,"PLAYER: x:"+$gamePlayer.x+", y:"+$gamePlayer.y+", dir:"+$gamePlayer._direction);
    //this.drawTextLine(3,);

    if(evt) {
        var id = evt._eventId;
        var dataEvent = $dataMap.events[id];
        this._currentEvent = {
            "dataEvent" : dataEvent,
            "mapEvent" : evt
        };
        this.drawTextLine(4,"EVENT-ID:"+id);
        this.drawTextLine(5, "x: "+dataEvent.x+", y: "+dataEvent.y+", dir: "+evt._direction)
        this.drawTextLine(6,"Name: "+dataEvent.name);
        this.drawTextLine(7,"Note: "+dataEvent.note);
    }

};

Window_GDTDebug.prototype.getEventInfo = function() {
   var y = $gamePlayer.y;
   var x = $gamePlayer.x;
   var dir = $gamePlayer._direction;

   var x2 = $gameMap.roundXWithDirection(x, dir);
   var y2 = $gameMap.roundYWithDirection(y, dir);
    if (!$gameMap.isEventRunning()) {
        return $gameMap.eventsXy(x2, y2).length > 0 ? $gameMap.eventsXy(x2, y2)[0] : null;
    } else {
        return null;
    }
};

Window_GDTDebug.prototype.drawTextLine = function(line, text) {
    var y = line > 0 ? (line-1) * this.lineHeight() : 0;
    this.drawTextEx(text, 10, y);
};

Window_GDTDebug.prototype.drawBackground = function(x, y, width, height) {
    var color1 = this.dimColor1();
    var color2 = 'rgba(0, 0, 0, 0.5)';
    this.contents.gradientFillRect(x, y, width/2 , height, color2, color1);
    this.contents.gradientFillRect(x + width/2, y, width / 2, height, color1, color2);
};

GDT.Base64Images = {};

ImageManager.loadCharacter = function(filename, hue) {
    if(filename.startsWith("base64")) {
        var name = filename.split("_")[1];
        return this.loadNormalBitmap(GDT.Base64Images[name]);
    } else {
        return this.loadBitmap('img/characters/', filename, hue, false);
    }

};

ImageManager.loadBitmap64 = function(filename, hue, smooth) {
    if (filename) {
        var bitmap = this.loadNormalBitmap(filename, hue || 0);
        bitmap.smooth = smooth;
        return bitmap;
    } else {
        return this.loadEmptyBitmap();
    }
};
