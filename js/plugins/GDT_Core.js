/*:
* @plugindesc v1.0 GDT Core
* @author Gamedev-Tutorials
*
* Just Core Functionality for Advanced Plugin Usage
*
*/

GDT = window.GDT || {};
GDT.Core = GDT.Core || {};
GDT.Core.version = 10;

GDT.Core.extractData = function(data) {
    var re = /<([^<>:]+)(:?)([^>]*)>/g;
    data.meta2 = {};
    for (;;) {
        var match = re.exec(data.note);
        if (match) {
            if (match[2] === ':') {
                if(data.meta2[match[1]]) {
                    if(!(data.meta2[match[1]] instanceof Array)) {
                        data.meta2[match[1]] = [data.meta2[match[1]]];
                    }
                    data.meta2[match[1]].push(match[3]);

                } else {
                    data.meta2[match[1]] = match[3];
                }
            } else {
                data.meta2[match[1]] = true;
            }
        } else {
            break;
        }
    }
};