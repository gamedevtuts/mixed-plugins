if(typeof GDT == "undefined") {
    GDT = {};
}
GDT.Base64Images = {};

GDT.uploadBase64 = function() {
  var input = document.createElement("input");
  input.type = "file";
  input.onchange = function() {
      GDT.uploadedBase64(input);
      document.querySelector("body").removeChild(input);
  };
  input.style = "visibility: hidden";
  document.querySelector("body").appendChild(input);
  input.click();
};
GDT.uploadedBase64Counter = 1;
GDT.uploadedBase64 = function(input) {
    const file = input.files[0];
    const reader = new FileReader();


    reader.addEventListener("load", function () {
        // convert image file to base64 string
        var img = document.createElement("img");
        img.src = reader.result;
        var sizePrefix = file.name.startsWith("$") ? "$" : "";
        GDT.Base64Images["hero"+GDT.uploadedBase64Counter] = reader.result;
        ImageManager.loadCharacter(sizePrefix+"base64_hero"+GDT.uploadedBase64Counter);

        var charSprite = null;
        SceneManager._scene._spriteset._characterSprites.map((elem, index) => {
            if(elem._characterName == $gamePlayer._characterName) {
                charSprite = elem}
        });
        $gamePlayer._characterName = sizePrefix+"base64_hero"+GDT.uploadedBase64Counter++;
        GDT.uploadedBase64Counter++;

    }, 'image/png');

    if (file) {
        reader.readAsDataURL(file);
    }
}

Game_Character.prototype.getSprite = function() {
  var name = this._characterName;
    var charSprite = null;
    SceneManager._scene._spriteset._characterSprites.map((elem, index) => {
        if(elem._characterName == name) {
            charSprite = elem}
    });
    return charSprite;
};

ImageManager.loadCharacter = function(filename, hue) {
    if(filename.indexOf("base64") > -1) {
        var name = filename.split("_")[1];
        return this.loadNormalBitmap(GDT.Base64Images[name]);
    } else {
        return this.loadBitmap('img/characters/', filename, hue, false);
    }

};

ImageManager.loadBitmap64 = function(filename, hue, smooth) {
    if (filename) {
        var bitmap = this.loadNormalBitmap(filename, hue || 0);
        bitmap.smooth = smooth;
        return bitmap;
    } else {
        return this.loadEmptyBitmap();
    }
};
